package com.example.scroll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()

            if (emailEditText.text.toString().isNotEmpty() && passwordEditText.text.toString().isNotEmpty()) {
                val intent = Intent(this, profileActivity::class.java)
                startActivity(intent)
            }
        }
    }
    private fun signUp() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() ) {
                signUpButton.isClickable = false
                if (email.matches(emailPattern.toRegex())) {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            signUpButton.isClickable = true
                            if (task.isSuccessful) {
                                d("signUp", "createUserWithEmail:success")
                                Toast.makeText(this, "SignUp is success!", Toast.LENGTH_SHORT)
                                    .show()
                                val user = auth.currentUser

                            } else {
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(
                                    this, task.exception.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                        }
                }else {
                    Toast.makeText(applicationContext, "Email format is not Correct",
                        Toast.LENGTH_SHORT).show()
                }

        } else {
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show()
        }

    }






}